# gitlab_qmake_gcc_cpp17_oclint

Branch   |GitLab CI                                                                                                                                                                                         |[![Codecov logo](pics/Codecov.png)](https://www.codecov.io)                                                                                                                                               
---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
`master` |[![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/commits/master)  |[![codecov.io](https://codecov.io/gitlab/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/coverage.svg?branch=master)](https://codecov.io/gitlab/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/branch/master)
`develop`|[![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/badges/develop/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/commits/develop)|[![codecov.io](https://codecov.io/gitlab/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/coverage.svg?branch=develop)](https://codecov.io/gitlab/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/branch/develop)

This GitLab is part of [the GitLab CI C++ Tutorial](https://gitlab.com/richelbilderbeek/gitlab_cpp_tutorial).

The goal of this project is to have a clean GitLab CI build, with specs:

 * C++ version: `C++17`
 * Build system: `qmake`
 * C++ compiler: `g++`
 * Libraries: `STL` only
 * Code coverage: None
 * Source: multiple files

Additionally, the code style is checked by OCLint.

More complex builds:

 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp20_oclint/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp20_oclint/commits/master) Use C++20: [gitlab_qmake_gcc_cpp20_oclint](https://www.gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp20_oclint)

Builds of similar complexity:

 * [![Build Status](https://travis-ci.org/richelbilderbeek/travis_qmake_gcc_cpp17_oclint.svg?branch=master)](https://travis-ci.org/richelbilderbeek/travis_qmake_gcc_cpp17_oclint) Use Travis CI: [travis_qmake_gcc_cpp17_oclint](https://www.github.com/richelbilderbeek/travis_qmake_gcc_cpp17_oclint)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/commits/master) Use GitLab CI: [gitlab_qmake_gcc_cpp17_oclint](https://www.gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint)
 * ![GitHub Actions](https://github.com/richelbilderbeek/gha_qmake_gcc_cpp17_oclint/workflows/check/badge.svg?branch=master) Use GitHub Actions: [gha_qmake_gcc_cpp17_oclint](https://www.github.com/richelbilderbeek/gha_qmake_gcc_cpp17_oclint)

Less complex builds:

 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/commits/master) Use C++17 without codecov: [gitlab_qmake_gcc_cpp17](https://www.gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17)

